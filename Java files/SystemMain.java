import java.util.ArrayList;

public class SystemMain {

	public SystemMain() {

	}

	ArrayList<Carer> carerList = new ArrayList<Carer>();
	ArrayList<Groomer> groomerList = new ArrayList<Groomer>();
	ArrayList<Manager> managerList = new ArrayList<Manager>();

	// create Groomer,Carer,Manager
	public Groomer createGroomer(String name, String surname, int emp) {
		Groomer g1 = new Groomer(name, surname, emp);
		groomerList.add(g1);	
		return g1;
	}

	public Carer createCarer(String name, String surname, int emp) {
		Carer c1 = new Carer(name, surname, emp);
		carerList.add(c1);
		return c1;
	}

	public Manager createManager(String name, String surname) {
		Manager m1 = new Manager(name, surname);
		managerList.add(m1);
		return m1;
	}
}
