import java.util.ArrayList;

public class Groomer {

	private String name;
	private String surname;
	private int employeeNumber;
	private String login;
	private String password;

	public Groomer(String name, String surname, int employeeNumber) {
		this.name = name;
		this.surname = surname;
		this.employeeNumber = employeeNumber;
	}

	// setters, getters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
