package com.dog;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import utilityClasses.DataGen;

@ManagedBean
@SessionScoped
public class SessionBean {
	private int id;
	private String username;
	private String password;
	public static UserList list = DataGen.generateEmployees();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String userName) {
		this.username = userName;
	}
	public String getPassword() {
		return this.password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String logout() {
		HttpSession session = SessionUtils.getSession();
		session.invalidate();
		return "managerLogin";
	}
	public String validateUsernamePassword() {
		boolean valid = list.logIn(username, password);
		System.out.print(this.password);
		System.out.print(list.getUser("Groomer2"));
		if (valid) {
			HttpSession session = SessionUtils.getSession();
			session.setAttribute("username", username);
			return "admin";
		} else {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_WARN,
							"Incorrect Username and Password",
							"Please enter correct username and Password"));
			return "managerLogin";
		}
	}
}
