package com.dog;

public class User {
	private String password;
	private String userName;
	private int id;
	
	public User() {
		this.userName = "user";
		this.password = "pass";
		this.id = 0;
	}
	public User(String name) {
		this.userName = name;
		this.password = "pass";
		this.id = -1;
	}
	public String getPassword() {
		return password;
	}
	public String getType() {
		String output ="";
		String [] userType = {"Manager","Groomer","Carer"};
		output = userType[this.getId()];
		return output;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public boolean logIn(String userName, String password) {
		boolean output = false;
		if(this.getPassword().equals(password)&&this.getUserName().equals(userName)) {
			output = true;
		}
		return output;
	}
	@Override
	public boolean equals(Object obj) {
		User comparedUser = (User) obj;
		boolean output =false;
		if (this.getUserName().equals(comparedUser.getUserName())) {
			output =true;
		}
		return output;
	}
}
