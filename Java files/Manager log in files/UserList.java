package com.dog;
import java.util.ArrayList;

public class UserList {
	private ArrayList<User> list;
	
	public UserList () {
		this.list = new ArrayList<User>();
	}
	
	public void setList(ArrayList<User> newList) {
		this.list = newList;
	}
	
	public ArrayList<User> getList(){
		return this.list;
	}
	
	public void addUser(User user) {
		this.list.add(user);
	}
	public int getSize() {
		return list.size();
	}
	public User getUser(int index) {
		return this.list.get(index);
	}
	
	public User getUser(String username) {
		User comparedUser = new User(username);
		User returnedUser = null;
		for (User user : list) {
			if (user.equals(comparedUser)) {
				returnedUser = user;
			}
		}
		return returnedUser;
	}
	public boolean logIn(String username, String password) {
		boolean output = false;
		if(this.getUser(username)!=null) {
			output = this.getUser(username).logIn(username, password);
		}
		return output;
	}
}
